#!/usr/bin/env python3
#
# re-crypt
#
# This file is part of re-crypt tool
# Copyright (C) 2023 Svyatoslav Ryhel <clamor95@gmail.com>
# Copyright (C) 2023 Ion Agorria <ion@agorria.com>
#

import ctypes
import json
import math

from ctypes import *

devtypes = {
    "nand": 1,
    "spi": 3,
    "sdmmc": 4,
}


class NAND_PARAMS_T30(Structure):
    _fields_ = [
        ("clock_divider", c_ubyte),
        ("async_timing0", c_uint),
        ("async_timing1", c_uint),
        ("async_timing2", c_uint),
        ("async_timing3", c_uint),
        ("sddr_timing0", c_uint),
        ("sddr_timing1", c_uint),
        ("tddr_timing0", c_uint),
        ("tddr_timing1", c_uint),
        ("fbio_dqsib_dly_byte", c_ubyte),
        ("fbio_quse_dly_byte", c_ubyte),
        ("fbio_cfg_quse_late", c_ubyte),
        ("disable_sync_ddr", c_ubyte),
        ("block_size_log2", c_ubyte),
        ("page_size_log2", c_ubyte)
    ]


class NAND_PARAMS_T20(Structure):
    _fields_ = [
        ("clock_divider", c_ubyte),
        ("nand_timing", c_uint),
        ("nand_timing2", c_uint),
        ("block_size_log2", c_ubyte),
        ("page_size_log2", c_ubyte)
    ]


sdmmc_data_width = {
    "1BIT": 0,
    "4BIT": 1,
    "8BIT": 2,
    "DDR_4BIT": 5,
    "DDR_4BIT": 6,
}


sdmmc_controller = {
    "SDMMC4": 0,
    "SDMMC3": 1,
}


class SDMMC_PARAMS(Structure):
    _fields_ = [
        ("clock_divider", c_ubyte),
        ("data_width", c_uint),
        ("max_power_class_supported", c_ubyte),
        ("sd_controller", c_uint)
    ]


spi_clock_source = {
    "PllPOut0": 0,
    "PllCOut0": 1,
    "PllMOut0": 2,
    "ClockM": 3,
}


class SPIFLASH_PARAMS(Structure):
    _fields_ = [
        ("clock_source", c_uint),
        ("clock_divider", c_ubyte),
        ("read_command_type_fast", c_ubyte),
    ]


# Defines parameters of the boot device
class DEV_PARAMS_T30(Union):
    _fields_ = [
        ("nand_params", NAND_PARAMS_T30),
        ("sdmmc_params", SDMMC_PARAMS),
        ("spiflash_params", SPIFLASH_PARAMS),
        ("size", c_ubyte * 64)
    ]


class DEV_PARAMS_T20(Union):
    _fields_ = [
        ("nand_params", NAND_PARAMS_T20),
        ("sdmmc_params", SDMMC_PARAMS),
        ("spiflash_params", SPIFLASH_PARAMS),
    ]


dev_memory_type = {
    "ddr": 1,
    "lpddr": 2,
    "ddr2": 3,
    "lpddr2": 4,
    "ddr3": 5,
}


dev_memory_type_t124 = {
    "ddr": 0,
    "lpddr": 0,
    "ddr2": 0,
    "lpddr2": 1,
    "ddr3": 2,
}


# SDRAM timings
class SDRAM_PARAMS_T124(Structure):
    _fields_ = [
        ("memory_type", c_uint),
        ("timings", c_uint * 308)
    ]


class SDRAM_PARAMS_T30(Structure):
    _fields_ = [
        ("memory_type", c_uint),
        ("timings", c_uint * 191)
    ]


class SDRAM_PARAMS_T20(Structure):
    _fields_ = [
        ("memory_type", c_uint),
        ("timings", c_uint * 127)
    ]


# Badblock layout
class BADBLOCK_TABLE(Structure):
    _fields_ = [
        ("entries_used", c_uint),
        ("virtual_blk_size_log2", c_ubyte),
        ("block_size_log2", c_ubyte),
        ("bad_blks", c_ubyte * 512)
    ]


# Bootloader info (one per bootloader)
class BOOTLOADER_INFO(Structure):
    _fields_ = [
        ("version", c_uint),
        ("start_blk", c_uint),
        ("start_page", c_uint),
        ("length", c_uint),
        ("load_addr", c_uint),
        ("entry_point", c_uint),
        ("attribute", c_uint),
        ("crypto_hash", c_uint * 4)
    ]


class BOOTLOADER_INFO_T124(Structure):
    _fields_ = [
        ("version", c_uint),
        ("start_blk", c_uint),
        ("start_page", c_uint),
        ("length", c_uint),
        ("load_addr", c_uint),
        ("entry_point", c_uint),
        ("attribute", c_uint),
        ("crypto_hash", c_uint * 4),
        ("bl_rsa_sig", c_uint * 64),
    ]


class BCT_T124(Structure):
    _fields_ = [
        ("badblock_table", BADBLOCK_TABLE),
        ("badblock_padding", c_ubyte * 6),
        ("rsa_key_modulus", c_uint * 64),
        ("crypto_hash", c_uint * 4),
        ("rsa_signature", c_uint * 64),
        ("customer_data", c_ubyte * 648),
        ("odm_data", c_uint),
        ("reserved1", c_uint),
        # Signed section of BCT
        ("random_aes_blk", c_uint * 4),
        ("unique_chip_id", c_uint * 4),
        ("boot_data_version", c_uint),
        ("block_size_log2", c_uint),
        ("page_size_log2", c_uint),
        ("partition_size", c_uint),
        ("num_param_sets", c_uint),
        ("dev_type", c_uint * 4),
        ("dev_params", DEV_PARAMS_T30 * 4),
        ("num_sdram_sets", c_uint),
        ("sdram_params", SDRAM_PARAMS_T124 * 4),
        ("bootloader_used", c_uint),
        ("bootloader", BOOTLOADER_INFO_T124 * 4),
        ("enable_fail_back", c_ubyte),
        ("secure_jtag_control", c_ubyte),
        ("reserved", c_ubyte * 2),
    ]


class BCT_T30(Structure):
    _fields_ = [
        ("crypto_hash", c_uint * 4),
        ("random_aes_blk", c_uint * 4),
        ("boot_data_version", c_uint),
        ("block_size_log2", c_uint),
        ("page_size_log2", c_uint),
        ("partition_size", c_uint),
        ("num_param_sets", c_uint),
        ("dev_type", c_uint * 4),
        ("dev_params", DEV_PARAMS_T30 * 4),
        ("num_sdram_sets", c_uint),
        ("sdram_params", SDRAM_PARAMS_T30 * 4),
        ("badblock_table", BADBLOCK_TABLE),
        ("bootloader_used", c_uint),
        ("bootloader", BOOTLOADER_INFO * 4),
        ("customer_data", c_ubyte * 2016),
        ("odm_data", c_uint),
        ("reserved1", c_uint),
        ("enable_fail_back", c_ubyte),
        ("reserved", c_ubyte * 3),
    ]


class BCT_T20(Structure):
    _fields_ = [
        ("crypto_hash", c_uint * 4),
        ("random_aes_blk", c_uint * 4),
        ("boot_data_version", c_uint),
        ("block_size_log2", c_uint),
        ("page_size_log2", c_uint),
        ("partition_size", c_uint),
        ("num_param_sets", c_uint),
        ("dev_type", c_uint * 4),
        ("dev_params", DEV_PARAMS_T20 * 4),
        ("num_sdram_sets", c_uint),
        ("sdram_params", SDRAM_PARAMS_T20 * 4),
        ("badblock_table", BADBLOCK_TABLE),
        ("bootloader_used", c_uint),
        ("bootloader", BOOTLOADER_INFO * 4),
        ("customer_data", c_ubyte * 1184),
        ("odm_data", c_uint),
        ("reserved1", c_uint),
        ("enable_fail_back", c_ubyte),
        ("reserved", c_ubyte * 3),
    ]


def get_soc(device):
    if device in ('tf101', 'paz00', 'a500'):
        return 't20'
    elif device in ('mocha'):
        return 't124'
    else:
        return 't30'


def get_entry(soc):
    if soc == 't20':
        return 0x00108000
    else:
        return 0x80108000


def create_bct(config, bootsize):
    soc = get_soc(config['device'])
    if soc == 't20':
        bct = BCT_T20()
    elif soc == 't124':
        bct = BCT_T124()
    else:
        bct = BCT_T30()
    bct_json_path = "bct/" + config['device'] + ".json"
    with open(bct_json_path) as file:
        obj = json.load(file)
    bct.boot_data_version = int(obj["Version"], 16)
    bct.block_size_log2 = int(math.log(int(obj["BlockSize"], 16), 2))
    bct.page_size_log2 = int(math.log(int(obj["PageSize"], 16), 2))
    bct.partition_size = int(obj["PartitionSize"], 16)
    # We need nothing from vendors!
    bct.odm_data = 0    # config['odm']
    # If bootdevice is SPI Flash then badblock table will be altered
    if soc == 't20':
        bct.badblock_table.entries_used = 0x400
    else:
        bct.badblock_table.entries_used = 0x800
    bct.badblock_table.virtual_blk_size_log2 = 0xE
    bct.badblock_table.block_size_log2 = 0xE
    # All checked devices have this set (no boot if removed).
    # The purpose is unclear.
    bct.reserved[0] = 0x80
    if "BootDev" in obj:
        bct.num_param_sets = len(obj["BootDev"])
    else:
        bct.num_param_sets = 0
    if bct.num_param_sets != 0:
        for i, dev_param in enumerate(obj["BootDev"]):
            dev_type = dev_param["DevType"]
            bct.dev_type[i] = devtypes[dev_type]
            if dev_type == "spi":
                dev_type_params = bct.dev_params[i].spiflash_params
                dev_type_params.read_command_type_fast = int(dev_param["ReadCommandTypeFast"], 16)
                dev_type_params.clock_source = spi_clock_source[dev_param["ClockSource"]]
                dev_type_params.clock_divider = int(dev_param["ClockDivider"], 16)
                bct.badblock_table.entries_used = 0x200
                bct.badblock_table.virtual_blk_size_log2 = 0xF
                bct.badblock_table.block_size_log2 = 0xF
            elif dev_type == "nand":
                dev_type_params = bct.dev_params[i].nand_params
                if soc == 't20':
                    dev_type_params.nand_timing = int(dev_param["NandTiming"], 16)
                    dev_type_params.nand_timing2 = int(dev_param["NandTiming2"], 16)
                else:
                    dev_type_params.async_timing0 = int(dev_param["NandAsyncTiming0"], 16)
                    dev_type_params.async_timing1 = int(dev_param["NandAsyncTiming1"], 16)
                    dev_type_params.async_timing2 = int(dev_param["NandAsyncTiming2"], 16)
                    dev_type_params.async_timing3 = int(dev_param["NandAsyncTiming3"], 16)
                    dev_type_params.sddr_timing0 = int(dev_param["NandSDDRTiming0"], 16)
                    dev_type_params.sddr_timing1 = int(dev_param["NandSDDRTiming1"], 16)
                    dev_type_params.tddr_timing0 = int(dev_param["NandTDDRTiming0"], 16)
                    dev_type_params.tddr_timing1 = int(dev_param["NandTDDRTiming1"], 16)
                    dev_type_params.fbio_dqsib_dly_byte = int(dev_param["NandFbioDqsibDlyByte"], 16)
                    dev_type_params.fbio_quse_dly_byte = int(dev_param["NandFbioQuseDlyByte"], 16)
                    dev_type_params.fbio_cfg_quse_late = int(dev_param["NandFbioCfgQuseLate"], 16)
                    dev_type_params.disable_sync_ddr = int(dev_param["DisableSyncDDR"], 16)
                dev_type_params.clock_divider = int(dev_param["ClockDivider"], 16)
                dev_type_params.block_size_log2 = int(dev_param["BlockSizeLog2"], 16)
                dev_type_params.page_size_log2 = int(dev_param["PageSizeLog2"], 16)
            elif dev_type == "sdmmc":
                dev_type_params = bct.dev_params[i].sdmmc_params
                dev_type_params.clock_divider = int(dev_param["ClockDivider"], 16)
                dev_type_params.data_width = sdmmc_data_width[dev_param["DataWidth"]]
                dev_type_params.max_power_class_supported = int(dev_param["MaxPowerClassSupported"], 16)
                dev_type_params.sd_controller = sdmmc_controller[dev_param["SdController"]]
            else:
                raise ValueError("Unknown DevType '{}' for BootDev {}".format(dev_type, i))
    if soc == 't124':
        bct.badblock_table.entries_used = 0x80
        bct.secure_jtag_control = int(obj["JtagCtrl"], 16)
        # Mi Pad stuff, and maybe some other T124
        if config['device'] in ('mocha'):
            bct.badblock_table.entries_used = 0x1000
            bct.badblock_table.virtual_blk_size_log2 = 0xF
        for l, ecid in enumerate(obj["ChipUid"]):
	        bct.unique_chip_id[l] = int(ecid, 16)
    bct.num_sdram_sets = len(obj["SDRAM"])
    for j, sdram_param in enumerate(obj["SDRAM"]):
        if soc == 't124':
            bct.sdram_params[j].memory_type = dev_memory_type_t124[sdram_param["MemoryType"]]
        else:
            bct.sdram_params[j].memory_type = dev_memory_type[sdram_param["MemoryType"]]
        for k, timing in enumerate(sdram_param["Timings"]):
            bct.sdram_params[j].timings[k] = int(timing, 16)
    # Bootloader is filled via parsed arguments
    bct.bootloader_used = 1
    bct.bootloader[0].version = 1
    bct.bootloader[0].start_blk = int(bootsize / int(obj["BlockSize"], 16))
    bct.bootloader[0].start_page = 0
    bct.bootloader[0].length = config['length']
    bct.bootloader[0].load_addr = get_entry(soc)
    bct.bootloader[0].entry_point = get_entry(soc)
    bct.bootloader[0].attribute = 0
    bct.bootloader[0].crypto_hash = config['crypto_hash']
#    with open('test.bct', 'wb') as output:
#        output.write(bytes(bct))
    return bytes(bct)
